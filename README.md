# Fontcustom Docker
Docker image using fontcustom to generate font.

## Requirements
- Docker

## Getting started
Build and run.

```bash
# Build
$ docker build -t fontcustom .
# or
$ docker build -t fontcustom -f Dockerfile.pre .

# Run
$ docker run -it fontcustom
```

## How to use
Add build job in your `.gitlab-ci.yml` as below.

```yml
stages:
  - build

font:
 stage: build
 image: registry.gitlab.com/filpals/docker-fontcustom:latest
 script:
 - fontcustom compile
 artifacts:
  paths:
  - app/assets/fonts
```

## References
1. https://docs.gitlab.com/ee/ci/docker/using_docker_images.html
2. https://medium.com/devops-with-valentine/how-to-build-a-docker-image-and-push-it-to-the-gitlab-container-registry-from-a-gitlab-ci-pipeline-acac0d1f26df
3. https://github.com/docker-library/ruby
