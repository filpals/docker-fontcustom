FROM ruby:2.6-alpine3.15

# Install build-essential and prerequisites
RUN apk update && apk add --virtual build-dependencies build-base bash gcc wget git
RUN git clone https://github.com/bramstein/sfnt2woff-zopfli.git sfnt2woff-zopfli && cd sfnt2woff-zopfli && make && mv sfnt2woff-zopfli /usr/local/bin/sfnt2woff
RUN git clone --recursive https://github.com/google/woff2.git && cd woff2 && make clean all && mv woff2_* /usr/local/bin/
RUN apk add --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/main python3
RUN apk add --no-cache --repository http://dl-cdn.alpinelinux.org/alpine/edge/testing fontforge
RUN gem install fontcustom

# Copy all files at current location into container
WORKDIR /
COPY . .

CMD ["bash"]